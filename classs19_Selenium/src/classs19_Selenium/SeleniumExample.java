package classs19_Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumExample {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver","C:\\Java\\chromedriver.exe");
		WebDriver browser;
		browser = new ChromeDriver();
		
		browser.get("https://sit.skyschooling.com/selenium01/");
		
		WebElement Id=browser.findElement(By.id("firstName"));
		Id.sendKeys("Salim");
		WebElement Name =browser.findElement(By.name("LastName"));
		Name.sendKeys("Miah");
		WebElement Name1 = browser.findElement(By.name("EmailID"));
		Name1.sendKeys("abc@gmail.com");
		WebElement Name2 = browser.findElement(By.name("MobileNumber"));
		Name2.sendKeys("3476596674");
		WebElement Name3 = browser.findElement(By.name("Gender"));
		Name3.click();
		WebElement id2 = browser.findElement(By.id("Birthday_Day"));
		id2.click();
		WebElement xpath = browser.findElement(By.xpath("//option[@value='15']"));
		xpath.click();
		WebElement xpath1 =browser.findElement(By.xpath("//option[@value='June']"));
		xpath1.click();
		WebElement xpath2 = browser.findElement(By.xpath("//option[@value='2001']"));
		xpath2.click();
		WebElement name4=browser.findElement(By.xpath("//tr[7]/td[2]/textarea[@name='Address']"));
		name4.sendKeys("123/3 New york,USA");
		WebElement xpath3=browser.findElement(By.xpath("//tr[8]/td[2]/textarea[@name='Address']"));
		xpath3.sendKeys("1150 E 56st New york,USA");
		
		WebElement name5 =browser.findElement(By.name("City"));
		name5.sendKeys("Brooklyn");
		WebElement name6 = browser.findElement(By.name("ZipCode"));
		name6.sendKeys("11234");
		WebElement name7 = browser.findElement(By.name("State"));
		name7.sendKeys("New York");
		WebElement name8 = browser.findElement(By.name("Country"));
		name8.sendKeys("USA");
		//WebElement name9 = browser.findElement(By.name("HobbyDrawing"));
		//name9.click();
		WebElement xpath4=browser.findElement(By.xpath("//input[@name='HobbyDrawing']"));
		xpath4.click();
		WebElement name10 = browser.findElement(By.name("HobbyDancing"));
		name10.click();
		WebElement name11 = browser.findElement(By.name("HobbyOther"));
		name11.click();
		WebElement name12= browser.findElement(By.name("Other_Hobby"));
		name12.sendKeys("other12345");
		WebElement id3=browser.findElement(By.id("submit_result"));
		id3.click();
	}

}
